const express = require("express");

const server = express();
server.use(express.json());

const projects = [];
var reqCount = 0;

// global middleware to register made requests
server.use((req, res, next) => {
  reqCount++;
  console.log(reqCount);
  next();
});

// middleware to check if the project with the provided id exists
function checkProject(req, res, next) {
  const { id } = req.params;

  const project = projects.find((item, index) => {
    if (item.id == id) {
      req.project = item;
      req.index = index;
      return item;
    }
  });

  if (!project) {
    res.status(404).json({ message: "Projeto não encontrado!" });
  }

  next();
}

// get all projects
server.get("/projects", (req, res) => {
  return res.json(projects);
});

// save a new project
server.post("/projects", (req, res) => {
  const { id, title, tasks } = req.body;
  const project = { id, title, tasks };

  projects.push(project);
  return res.status(200).send();
});

// update a project with a given id
server.put("/projects/:id", checkProject, (req, res) => {
  const { title } = req.body;
  const { project, index } = req;

  project.title = title;
  projects[index] = project;

  return res.status(200).json(project);
});

// delete a project with a given id
server.delete("/projects/:id", checkProject, (req, res) => {
  projects.splice(req.index, 1);

  return res.status(200).json();
});

// add tasks to a project with a given id
server.post("/projects/:id/tasks", checkProject, (req, res) => {
  const task = req.body.title;
  const { project, index } = req;

  project.tasks.push(task);
  projects[index] = project;

  return res.status(200).json(project.tasks);
});

server.listen(3000);
